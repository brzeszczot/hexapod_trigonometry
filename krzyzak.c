#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define degreesToRadians(angleDegrees) (angleDegrees * M_PI / 180.0)
#define radiansToDegrees(angleRadians) (angleRadians * 180.0 / M_PI)

#define TIBIA           60
#define FEMUR           35
#define COXA            13

#define BODY_OFFSET_X   30
#define BODY_OFFSET_Z   63

typedef struct s_xyz
{
    double x, y, z;
} xyz;

void LegIK(double x, double y, double z)
{
    double leg_length, hf, alfa, alfa1, alfa2, beta1, femur_angle, tibia_angle, coxa_angle;

    // calculating arm angles
    leg_length = sqrt(pow(x, 2) + pow(z, 2));
    hf = sqrt(pow(leg_length - COXA, 2) + pow(y, 2));
    alfa1 = atan2(leg_length - COXA, y);
    alfa2 = acos((pow(TIBIA, 2) - pow(FEMUR, 2) - pow(hf, 2)) / (-2 * FEMUR * hf));
    alfa = alfa1 + alfa2;
    femur_angle = 90 - radiansToDegrees(alfa);
    beta1 = acos((pow(hf, 2) - pow(TIBIA, 2) - pow(FEMUR, 2)) / (-2 * FEMUR * TIBIA));
    tibia_angle = 90 - radiansToDegrees(beta1);
    //coxa_angle = radiansToDegrees(atan2(z, x));
    coxa_angle = radiansToDegrees(atan2(z, x));

    printf("coxa_angle: %lf, femur_angle: %lf, tibia_angle: %lf\n", coxa_angle, femur_angle, tibia_angle);
}

xyz BodyIK(double x, double y, double z, double angle)
{
    xyz dim;

    dim.x = (cos(degreesToRadians(angle)) * x) + (sin(degreesToRadians(angle)) * z);
    dim.z = -(sin(degreesToRadians(angle)) * x) + (cos(degreesToRadians(angle)) * z);
    dim.y = 0;

    printf("Matrix rotation: x=%lf, y=%lf, z=%lf, angle=%lf\n", dim.x, y, dim.z, angle);

    return dim;
}

int main(int argc, char** argv)
{
    double x, y ,z, angle, v_x, v_y, v_z;

    if(argc <= 1)
    {
        printf("Usege: ./krzyzak current_x, current_y, current_z, transform_angle, vector_x, vector_y, vector_z\nExample: ./krzyzak 48 60 0 -30 -10 0 -17\n");
        return 1;
    }

    x = atof(argv[1]);
    y = atof(argv[2]);
    z = atof(argv[3]);
    angle = atof(argv[4]);
    v_x = atof(argv[5]);
    v_y = atof(argv[6]);
    v_z = atof(argv[7]);

    printf("Start: x=%lf, y=%lf, z=%lf, angle=%lf\n", x, y, z, angle);

    LegIK(x, y, z);

    xyz trans;
    trans = BodyIK(v_x, v_y, v_z, angle);

    printf("Matrix rotation DONE: z=%lf, y=%lf, x=%lf, angle=%lf\n", x + trans.z, y, (z + trans.x)* -1 , angle);

    LegIK(x + trans.z, y, z + trans.x);

    return 0;
}
